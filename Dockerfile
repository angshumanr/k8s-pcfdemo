FROM redhat/ubi8

RUN yum -y update
RUN yum -y remove java
RUN yum install -y \
       java-1.8.0-openjdk \
       java-1.8.0-openjdk-devel
CMD ["java", "-version"]
WORKDIR /
#RUN yum update -y
#RUN yum install -y git
#CMD ["git", "--version"]
#RUN git clone https://angshumanr:A1r2a3y4@gitlab.com/angshumanr/dockerdemo.git
#RUN git clone https://gitlab.rosetta.ericssondevops.com/sd-mana-tmo-prepaid-cdp-pristine/ppm-apps/eie/eie_utilities.git
RUN mkdir -p /opt/wsil/eie/certs
ADD src/KEYSTORE_TMO_CERT /opt/wsil/eie/certs/KEYSTORE_TMO_CERT
ADD src/bootstrap.yml bootstrap.yml
ADD src/GetPrepaidBalanceWSILMS-config.yml GetPrepaidBalanceWSILMS-config.yml
ADD src/GetPrepaidBalanceWSILMS-1.0.jar GetPrepaidBalanceWSILMS-1.0.jar
EXPOSE 8083
ENTRYPOINT exec java -jar GetPrepaidBalanceWSILMS-1.0.jar --spring.config.location=file:./bootstrap.yml,./GetPrepaidBalanceWSILMS-config.yml